﻿using Biblioteca.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Biblioteca.Controllers
{
    public class LibroController : Controller
    {
        private BibliotecaBdModel bd = new BibliotecaBdModel();
        // GET: Libro
        public ActionResult Index()
        {
            return View(bd.libro.ToList());
        }
        
        public ActionResult New()
        {
            return View();
        }
       
    }
}