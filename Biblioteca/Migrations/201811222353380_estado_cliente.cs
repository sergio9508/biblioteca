namespace Biblioteca.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class estado_cliente : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.estadoClientes",
                c => new
                    {
                        idEstadoCliente = c.Int(nullable: false, identity: true),
                        descripcion = c.String(),
                    })
                .PrimaryKey(t => t.idEstadoCliente);
            
            AddColumn("dbo.cliente", "idEstadoCliente", c => c.Int());
            CreateIndex("dbo.cliente", "idEstadoCliente");
            AddForeignKey("dbo.cliente", "idEstadoCliente", "dbo.estadoClientes", "idEstadoCliente");
            DropColumn("dbo.cliente", "estado");
        }
        
        public override void Down()
        {
            AddColumn("dbo.cliente", "estado", c => c.String(maxLength: 10, fixedLength: true));
            DropForeignKey("dbo.cliente", "idEstadoCliente", "dbo.estadoClientes");
            DropIndex("dbo.cliente", new[] { "idEstadoCliente" });
            DropColumn("dbo.cliente", "idEstadoCliente");
            DropTable("dbo.estadoClientes");
        }
    }
}
