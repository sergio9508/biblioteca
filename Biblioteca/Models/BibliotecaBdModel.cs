namespace Biblioteca.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class BibliotecaBdModel : DbContext
    {
        public BibliotecaBdModel()
            : base("name=Biblioteca")
        {
        }

        public virtual DbSet<cliente> cliente { get; set; }
        public virtual DbSet<libro> libro { get; set; }
        public virtual DbSet<prestamo> prestamo { get; set; }
        public DbSet<estadoCliente> estadoClientes { get; set; } 
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
         

            modelBuilder.Entity<libro>()
                .Property(e => e.estado)
                .IsFixedLength();

            modelBuilder.Entity<prestamo>()
                .Property(e => e.estado)
                .IsFixedLength();
        }
    }
}
