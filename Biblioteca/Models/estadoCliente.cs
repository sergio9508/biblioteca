﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Biblioteca.Models
{
    public class estadoCliente
    {
        [Key]
        public int idEstadoCliente { get; set; }

        public String descripcion { get; set; }

        public List<cliente> Clienetes { get; set; }

    }
}