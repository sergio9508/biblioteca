namespace Biblioteca.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("prestamo")]
    public partial class prestamo
    {
        [Key]
        public int idPrestamo { get; set; }

        public int? idCliente { get; set; }

        public int? idLibro { get; set; }

        [Column(TypeName = "date")]
        public DateTime? fecha_salida { get; set; }

        [StringLength(10)]
        public string estado { get; set; }

        [Column(TypeName = "date")]
        public DateTime? fecha_entrada { get; set; }

        public virtual cliente cliente { get; set; }

        public virtual libro libro { get; set; }
    }
}
