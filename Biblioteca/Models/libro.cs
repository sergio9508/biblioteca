namespace Biblioteca.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("libro")]
    public partial class libro
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public libro()
        {
            prestamo = new HashSet<prestamo>();
        }

        [Key]
        public int idLibro { get; set; }

        [StringLength(50)]
        [Display (Name ="Titulo")]
        public string titulo { get; set; }

        [StringLength(50)]
        [Display(Name = "Autor")]
        public string autor { get; set; }

        [Display(Name = "Paginas")]
        public int? paginas { get; set; }

        [Display(Name = "Estado")]
        [StringLength(10)]
        public string estado { get; set; }

        [Display(Name = "Fecha de publicacion")]
        [Column(TypeName = "date")]
        [DisplayFormat( DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime fecha_pub { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<prestamo> prestamo { get; set; }
    }
}
